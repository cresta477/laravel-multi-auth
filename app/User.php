<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profiles(){
        return $this->belongsToMany('App\Profile');
    }

    public function authorizeProfiles($profiles){
     if ($this->hasAnyProfile($profiles)) {
        return true;
    }
  return false;
}

   public function hasAnyProfile($profiles){
      if (is_array($profiles)) {
        foreach ($profiles as $profile) {
          if ($this->hasProfile($profile)) {
            return true;
          }
        }
      } else {
        if ($this->hasProfile($profiles)) {
          return true;
        }
      }
      return false;
    }

    public function hasProfile($profile){
        // var_dump ($this->profiles()->get);
        // die();
      if ($this->profiles()->where('name', $profile)->first()) {
        return true;
      }
      return false;
    }
}
