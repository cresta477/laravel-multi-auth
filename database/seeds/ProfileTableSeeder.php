<?php

use Illuminate\Database\Seeder;
use App\Profile;
class ProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_employee = new Profile();
	    $role_employee->name = 'employee';
	    $role_employee->remarks = 'A Employee User';
	    $role_employee->save();

	    $role_manager = new Profile();
	    $role_manager->name = 'manager';
	    $role_manager->remarks = 'A Manager User';
	   
	    $role_manager->save();
    }
}
