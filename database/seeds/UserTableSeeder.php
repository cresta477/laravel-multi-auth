<?php


use Illuminate\Database\Seeder;
use App\Profile;
use App\User;


class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_employee = Profile::where('name', 'employee')->first();
	    $role_manager  = Profile::where('name', 'manager')->first();

	    $employee = new User();
	    $employee->name = 'bikram';
	    $employee->email = 'cresta@employee.com';
	    $employee->password = bcrypt('secret');
	    $employee->created_by = 1;
	    $employee->save();
	    $employee->profiles()->attach($role_employee);

	    $manager = new User();
	    $manager->name = 'cresta';
	    $manager->email = 'cresta@manager.com';
	    $manager->password = bcrypt('secret');
	    $manager->created_by = 1;
	    $manager->save();
	    $manager->profiles()->attach($role_manager);
    }
}
